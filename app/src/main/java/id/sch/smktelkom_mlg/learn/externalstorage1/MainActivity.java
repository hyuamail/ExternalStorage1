package id.sch.smktelkom_mlg.learn.externalstorage1;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;

public class MainActivity extends AppCompatActivity
{

    public static final String FILE_NAME = "data.txt";
    private TextView tv;
    private AlertDialog addDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                showAddDialog();
            }
        });

        FloatingActionButton fab2 = (FloatingActionButton) findViewById(R.id.fab2);
        fab2.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                deleteFile();
            }
        });

        initText();

        initDialog();
    }

    private void initDialog()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Add Text")
                .setView(R.layout.add_dialog)
                .setPositiveButton("Add", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        addText();
                    }
                });

        addDialog = builder.create();
    }

    private void deleteFile()
    {
        if (FileExUtil.isExternalStorageWritable())
        {
            if (!FileExUtil.deleteFile(this, FILE_NAME))
            {
                Toast.makeText(this, "DELETE FAIL", Toast.LENGTH_LONG);
            }
            showText();
        }
        else
            Toast.makeText(this, "CAN'T WRITE TO EXTERNAL STORAGE", Toast.LENGTH_LONG);
    }

    private void initText()
    {
        tv = (TextView) findViewById(R.id.textView);
        showText();
    }

    private void showText()
    {
        String text = null;

        try
        {
            text = FileExUtil.checkNReadString(this, FILE_NAME);
        } catch (IOException e)
        {
            Toast.makeText(this, "ERROR READ:" + e.getMessage(), Toast.LENGTH_LONG);
        }

        tv.setText(text);
    }

    private void showAddDialog()
    {
        if (FileExUtil.isExternalStorageWritable())
            addDialog.show();
        else
            Toast.makeText(this, "CAN'T WRITE TO EXTERNAL STORAGE", Toast.LENGTH_LONG);
    }

    private void addText()
    {
        EditText etAdd = (EditText) addDialog.findViewById(R.id.editTextAdd);
        addToFile(FILE_NAME, etAdd.getText().toString());
        etAdd.setText(null);
        showText();
    }

    private void addToFile(String fileName, String data)
    {
        try
        {
            FileExUtil.createOrAppendString(this, fileName, data);
        } catch (IOException e)
        {
            Toast.makeText(this, "ERROR WRITE:" + e.getMessage(), Toast.LENGTH_LONG);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings)
        {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
