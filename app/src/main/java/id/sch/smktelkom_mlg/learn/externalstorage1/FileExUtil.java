package id.sch.smktelkom_mlg.learn.externalstorage1;

import android.content.Context;
import android.os.Environment;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created by hyuam on 01/03/2017.
 */

public class FileExUtil
{
    public static void writeString(Context context, String fileName, String data) throws IOException
    {
        File file = new File(context.getExternalFilesDir(null), fileName);

        FileOutputStream fos = new FileOutputStream(file);
        fos.write(data.getBytes());
        fos.close();
    }

    public static void appendString(Context context, String fileName, String data)
            throws IOException
    {
        File file = new File(context.getExternalFilesDir(null), fileName);

        FileOutputStream fos = new FileOutputStream(file, true);
        fos.write(data.getBytes());
        fos.close();
    }

    public static String readString(Context context, String fileName) throws IOException
    {
        File file = new File(context.getExternalFilesDir(null), fileName);

        BufferedReader br = new BufferedReader(new FileReader(file));

        StringBuilder data = new StringBuilder();
        String line;
        while ((line = br.readLine()) != null)
        {
            data.append(line);
            data.append('\n');
        }

        br.close();

        return data.toString();
    }

    public static boolean isExist(Context context, String fileName)
    {
        File file = new File(context.getExternalFilesDir(null), fileName);
        return file.exists();
    }

    public static String checkNReadString(Context context, String fileName) throws IOException
    {
        if (isExist(context, fileName))
        {
            return readString(context, fileName);
        }
        return null;
    }

    public static void createOrAppendString(Context context, String fileName, String data)
            throws IOException
    {
        if (isExist(context, fileName))
        {
            appendString(context, fileName, data);
        }
        else
        {
            writeString(context, fileName, data);
        }
    }

    public static boolean deleteFile(Context context, String fileName)
    {
        File file = new File(context.getExternalFilesDir(null), fileName);
        if (file != null)
            return file.delete();
        return false;
    }

    /* Checks if external storage is available for read and write */
    public static boolean isExternalStorageWritable()
    {
        String state = Environment.getExternalStorageState();
        return Environment.MEDIA_MOUNTED.equals(state);
    }

    /* Checks if external storage is available to at least read */
    public static boolean isExternalStorageReadable()
    {
        String state = Environment.getExternalStorageState();
        return Environment.MEDIA_MOUNTED.equals(state) ||
                Environment.MEDIA_MOUNTED_READ_ONLY.equals(state);
    }
}
